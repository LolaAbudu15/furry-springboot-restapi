package com.conygre.spring.example.controllers;

import java.util.Collection;

import com.conygre.spring.example.entities.Furry;
import com.conygre.spring.example.service.FurryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

//@RestController enables Spring to create it as a Controller
//@RequestMapping sets the url
// "/furries" is the value we are providing
@RestController
@RequestMapping("/furries")
public class FurryController {

    //@Autowired enables SPring to instantiate a new FurrySerice, 
    // so we dont have to do FurryService service = new FurryService();
    @Autowired
    private FurryService service;
    
    // GET method returns a collection of Furries
    @RequestMapping(method=RequestMethod.GET)
    public Collection<Furry> getFurries() {
        return service.getFurries();
    }

    // POST method addes things into the DB/Repository
    @RequestMapping(method=RequestMethod.POST)
    public void addFurry(@RequestBody Furry furry){
        service.addFurry(furry);
    }
    
}