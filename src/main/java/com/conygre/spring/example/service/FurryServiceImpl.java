package com.conygre.spring.example.service;

import java.util.Collection;

import com.conygre.spring.example.entities.Furry;
import com.conygre.spring.example.repo.FurryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//this is the implementation of the FurryService interface
//@Service annotates it as a service class - aka the Business layer
//this service layer needs a property so we will autowire it to the FurryRepository
@Service
public class FurryServiceImpl implements FurryService {

    // @Autowired causes Spring to inject an instance of the 
    //   FurryRepository class within this class
    @Autowired
    private FurryRepository repo;

    @Override
    public void addFurry(Furry furry) {
        //adds the furry into DB
        //can add other checks to validate here (like if/else checks)
        repo.insert(furry);
    }

    @Override
    public Collection<Furry> getFurries() {
        // returns/gets all the furries
        return repo.findAll();
    }
}